import { browser, logging, by, element, ProtractorBrowser, ElementFinder } from 'protractor';

describe('User Authentication', () => {
  beforeEach(async () => {
    await browser.get(browser.baseUrl);
  });

  // Check the inital page state
  describe('Given that I navigate to the login page', () => {
    it('Then the page title should contain the text "login"', async () => {
      expect(await browser.getTitle()).toContain('login');
    });

    // it('Then the page contains a field named userName', () => {
    //   const inputUserName = element(by.css('[name=userName]'));

    //   expect(inputUserName.isPresent()).toBeTrue();

    //   it('And the placeholder should contain the text "login"', () => {
    //     expect(inputUserName.getTitle()).toContain('login');
    //   });
    // });

    // it('Then the page contains a field named password', () => {
    //   const inputPassword = element(by.css('[name=password]'));

    //   expect(inputPassword.isPresent()).toBeTrue();

    //   it('And the placeholder should contain the text "password"', () => {
    //     expect(inputPassword.getTitle()).toContain('password');
    //   });
    // });

    // it('Then the page contain a button with the text login', () => {
    //   const buttonLogin = element(by.partialButtonText('login'));

    //   expect(buttonLogin.isPresent()).toBeTrue();
    // });

    // it('Then the page contains a link containing the text "forgotten"', () => {
    //   expect(by.partialLinkText('forgotton')).toBeTruthy();
    // });
  });
});


afterEach(async () => {
  // Assert that there are no errors emitted from the browser
  const logs = await browser.manage().logs().get(logging.Type.BROWSER);

  expect(logs).not.toContain(jasmine.objectContaining({
    level: logging.Level.SEVERE,
  } as logging.Entry));
});
