import { Component, OnInit } from '@angular/core';

class LoginDetails {
  public userName: string;
  public password: string;
  public rememberMe: boolean;

  constructor() { }
}

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  public loginForm: LoginDetails;

  constructor() {
    this.loginForm = new LoginDetails();
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log(this.loginForm);
  }
}
