import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject, ReplaySubject } from 'rxjs';

export enum AuthServiceStatus {
  Ready = 'Ready',
  LoggedIn = 'LoggedIn',
  AuthenticationError = 'AuthenticationError',
  NetworkError = 'NetworkError',
  Loading = 'Loading'
}

enum AuthReponseResult {
  MultipleMatch = 'MultipleMatch',
  MemberMissing = 'MemberMissing',
  AccountAutoLocked = 'AccountAutoLocked',
  AccountManuallyLocked = 'AccountManuallyLocked',
  Success = 'Success',
  NotMatched = 'NotMatched',
  GeneralFailure = 'GeneralFailure',
  PinError = 'PinError',
  CallCenterLoginFailure = 'CallCenterLoginFailure',
}

interface IAuthReponse {
  LoginInfo: {
    Result: AuthReponseResult;
    LoginAttempts: number;
    MemberID: number;
    TokenValue: string;
    ForcedPasswordReset: boolean;
    ParentalConsentRequired: boolean;
  };
}

interface IAuthRequest {
  identificationType: string;
  identificationValue: string;
  validationType: string;
  validationValue: string;
  defaultSite: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public readonly status = new ReplaySubject<AuthServiceStatus>(1);

  constructor(private http: HttpClient) {
    this.status.next(AuthServiceStatus.Ready);
  }

  public login(userName: string, password: string) {
    this.status.next(AuthServiceStatus.Loading);

    if (userName && !userName.trim()) {
      throw new Error('username cannot be empty');
    }

    if (!password) {
      throw new Error('password cannot be empty');
    }

    const body = <IAuthRequest>{};

    return this.http.post(environment.api.loginMemberUrl, body).subscribe((data: IAuthReponse) => {
      try {
        if (data.LoginInfo.Result === AuthReponseResult.Success) {
          this.status.next(AuthServiceStatus.LoggedIn);
        }
        else {
          this.status.next(AuthServiceStatus.AuthenticationError);
        }
      } catch (error) {
        throw new Error(error);
      }
    }, error => {
      this.status.next(AuthServiceStatus.NetworkError);
    });
  }
}
