import { AuthServiceStatus } from './auth.service';
import { environment } from 'src/environments/environment';
import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AuthServiceService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });

  describe('Given the initial state', () => {
    it('Then should be created', () => {
      expect(service).toBeTruthy();
    });

    it('Then status should be Ready', done => {
      service.status.subscribe(next => {
        expect(next).toEqual(AuthServiceStatus.Ready);

        done();
      });
    });
  });

  describe('Given that login is invoked', () => {
    describe('When user name and password is null', () => {
      it('Then an error should be thrown', () => {
        expect(() => {
          service.login(null, null);
        }).toThrow();
      });
    });

    describe('When password is empty', () => {
      it('Then an error should be thrown', () => {
        expect(() => {
          service.login('username', '');
        }).toThrow();
      });
    });

    describe('When credentials are valid', () => {
      it('Then the request method should be POST', () => {
        service.login('username', 'password');

        const request = httpMock.expectOne(environment.api.loginMemberUrl);

        expect(request.request.method).toBe('POST');
      });

      it('Then user should be LoggedIn', done => {
        service.login('username', 'password');

        const request = httpMock.expectOne(environment.api.loginMemberUrl);

        request.flush({
          "LoginInfo": {
            "Result": "Success",
            "LoginAttempts": 1,
            "MemberID": 1,
            "TokenValue": "string",
            "ForcedPasswordReset": false,
            "ParentalConsentRequired": false
          }
        });

        httpMock.verify();

        service.status.subscribe(next => {
          expect(next).toEqual(AuthServiceStatus.LoggedIn);

          done();
        });
      });
    });

    describe('When user name does not exist', () => {
      it('Then user should not be logged in', done => {
        service.login('username', 'password');

        const request = httpMock.expectOne(environment.api.loginMemberUrl);

        request.flush({
          "LoginInfo": {
            "Result": "NotMatched",
            "LoginAttempts": 1,
            "MemberID": 1,
            "TokenValue": "string",
            "ForcedPasswordReset": false,
            "ParentalConsentRequired": false
          }
        }, {
          headers: {
            'Content-type': 'text/xml'
          }
        });

        httpMock.verify();

        it('Then status should be AuthenticationError', done => {
          service.status.subscribe(next => {
            expect(next).toEqual(AuthServiceStatus.AuthenticationError);

            done();
          });
        });
      });

      describe('When password is incorrect', () => {
        it('Then user should not be logged in', () => {
          const request = httpMock.expectOne(environment.api.loginMemberUrl);

          request.flush({
            "LoginInfo": {
              "Result": "GeneralFailure",
              "LoginAttempts": 1,
              "MemberID": 1,
              "TokenValue": "string",
              "ForcedPasswordReset": false,
              "ParentalConsentRequired": false
            }
          }, {
            headers: {
              'Content-type': 'text/xml'
            }
          });

          httpMock.verify();

          it('Then status should be AuthenticationError', done => {
            service.status.subscribe(next => {
              expect(next).toEqual(AuthServiceStatus.AuthenticationError);

              done();
            });
          });
        });
      });
    });
  });
});
